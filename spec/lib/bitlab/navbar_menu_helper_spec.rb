# encoding: utf-8
require 'spec_helper'
require 'action_view'
require 'active_support'
require_relative '../../../app/helpers/navbar_menu_helper'

include ActionView::Helpers
include ActionView::Context
include NavbarMenuHelper

describe NavbarMenuHelper, type: :helper do

  it 'should return a basic dropdown on navbar' do
    expect(navbar_dropdown("dropdown"){}.gsub(/\s/, '').downcase)
    .to eql(NAVBAR_DROPDOWN.gsub(/\s/, '').downcase)
  end

  it 'should return a li with class divider' do
    expect(navbar_divider.gsub(/\s/, '').downcase)
    .to eql(NAVBAR_DIVIDER.gsub(/\s/, '').downcase)
  end

  it 'should return a link button for navbar' do
    expect(navbar_link(nil, '#', 'ad ad-screen-full').gsub(/\s/, '').downcase)
    .to eql(NAVBAR_LINK.gsub(/\s/, '').downcase)
  end

  it 'should retu a footer link for dropdown' do
    expect(navbar_dropdown_footer('Logout', '#', 'fa fa-power-off')
            .gsub(/\s/, '').downcase)
    .to eql(NAVBAR_DROPDOWN_FOOTER.gsub(/\s/, '').downcase)
  end

  it 'should return a metro_link' do
    expect(metro_link('Messages', '#', 'bg-primary', 'glyphicon glyphicon-inbox')
    .gsub(/\s/, '').downcase).to eql METRO_LINK.gsub(/\s/, '').downcase
  end

  it 'should return a topbar_menu_toggle' do
    expect(topbar_menu_toggle('ad ad-wand').gsub(/\s/, '').downcase)
    .to eql(TOPBAR_MENU_TOGGLE.gsub(/\s/, '').downcase)
  end

  it 'should return a topbar_dropmenu' do
    expect(topbar_dropmenu{}.gsub(/\s/, '').downcase)
    .to eql(TOPBAR_DROPMENU.gsub(/\s/, '').downcase)
  end

end

NAVBAR_DROPDOWN = <<-HTML
  <li class="dropdown menu-merge hidden-xs"><a href="#" class="dropdown-toggle"
  data-toggle="dropdown" role="button" aria-expanded="false">Dropdown
  <span class="caret caret-tp"></span></a><ul class="dropdown-menu"
  role="menu"></ul></li>
HTML
NAVBAR_DIVIDER = <<-HTML
  <li class="menu-divider hidden-xs"><i class="fa fa-circle"></i></li>
HTML
NAVBAR_LINK = <<-HTML
  <li class="hidden-xs">
   <a class="toggle-active" href="#">
   <span class="ad ad-screen-full fs18"></span></a>
  </li>
HTML
NAVBAR_DROPDOWN_FOOTER = <<-HTML
<li class="dropdown-footer">
  <a class="" href="#">
  <span class="fa fa-power-off fs18"></span> Logout </a>
</li>
HTML
METRO_LINK = <<-HTML
  <a href="#" class="metro-tile bg-primary light"><span
  class="glyphicon glyphicon-inbox text-muted"></span><span
  class="metro-title">Messages</span>                  </a>
HTML
TOPBAR_MENU_TOGGLE = <<-HTML
<a class="topbar-menu-toggle btn btn-sm" data-toggle="button"
aria-pressed="false" href="#"><span class="ad ad-wand"></span></a>
HTML
TOPBAR_DROPMENU = <<-HTML
<div id="topbar-dropmenu" class="alt topbar-menu-open">
<div class="topbar-menu row">                                      </div></div>
HTML
