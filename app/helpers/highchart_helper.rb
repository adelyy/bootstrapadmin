module HighchartHelper
  def highchart(chart_params)
    msg_error = "Favor de importar highcharts"
    content_tag :div, msg_error, class: 'highcharts', data: chart_params
  end
end
