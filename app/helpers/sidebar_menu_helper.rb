module SidebarMenuHelper

  TITLE_CSS = 'sidebar-title '
  SIDEBAR_COLOR = 'sidebar-dark '
  LEFT_CSS = 'nano nano-light affix has-scrollbar '
  LEFT_CONTENT = 'sidebar-left-content nano-content '
  CONTENT_SIDEBAR = 'sidebar-left-content nano-content '
  ACCORDION_LINK = { li: { id: '', class: '' }, a: { id: '', class: '' } }
  ACCORDION = { li: {class: '', id: ''},
                a: {id: '', class: 'accordion-toggle'} }

  def sidebar_toggle
    content_tag :div, class: 'sidebar-toggle-mini' do
      content_tag :a, href: '#' do
        content_tag :span, nil, class: 'fa fa-sign-out'
      end
    end
  end

  def sidebar_menu(&block)
    content_tag :ul, class: 'nav sidebar-menu', &block
  end

  def sidebar_content(css={}, &block)
    css = {} unless css
    div_css = css.has_key?(:css) ? LEFT_CONTENT + css[:css] :  LEFT_CONTENT
    div_style = css.has_key?(:style) ? css[:style] : 'margin-right: -15px;'
    content_tag :div, class: div_css, tabindex:'0', style: div_style, &block
  end

  def nano_pane(style=nil)
    style = style || 'height: 210px; transform: translate(0px, 0px);'
    content_tag :div, class: 'nano-pane' do
      content_tag :div, nil,  class: 'nano-slider', style: style
    end
  end

  def sidebar(css={}, &block)
    color = (!css.nil? and css.has_key?(:color) )? LEFT_CSS + css[:color] : LEFT_CSS
    content_tag :aside, id: 'sidebar_left', class: color do
      concat sidebar_content css, &block
      concat nano_pane
    end
  end

  def sidebar_label(text)
    content_tag :li, text, class: 'sidebar-label pt20'
  end

  def sidebar_icon_link(title, url, icon, label, css=nil)
    content_tag :li, class: css do
      sidebar_simple_link(title, url, icon, label, {}, TITLE_CSS)
    end
  end

  def sidebar_image_link(title, url, path, label, css=nil)
    content_tag :li, class: css do
      sidebar_simple_link(title, url, path, label, {}, TITLE_CSS, 'image')
    end
  end

  def sidebar_icon_accordion(title, icon, css={}, &block)
    css = append_attributes(ACCORDION, css)
    content_tag :li, css[:li] do
      concat sidebar_simple_link(title, '#', icon, nil, css[:a], TITLE_CSS)
      concat content_tag :ul, class: 'nav sub-nav', &block
    end
  end

  def sidebar_image_accordion(title, path, css={}, &block)
    css = append_attribute(ACCORDION, css)
    content_tag :li, css[:li] do
      concat sidebar_simple_link(title, '#', path, nil, css[:a], TITLE_CSS, 'image')
      concat content_tag :ul, class: 'nav sub-nav', &block
    end
  end

  def accordion_icon_link(title, url, icon, label, css={})
    css = append_attributes(ACCORDION_LINK, css)
    content_tag :li, css[:li] do
      sidebar_simple_link(title, url, icon, label, css[:a], nil)
    end
  end

  def accordion_image_link(title, url, path, label, css={})
    css = append_attributes(ACCORDION_LINK, css)
    content_tag :li, css[:li] do
      sidebar_simple_link(title, url, path, label, css[:a], nil, 'image')
    end
  end

  def sidebar_status
    # <li class="sidebar-stat">
    #   <a href="#projectOne" class="fs11">
    #     <span class="fa fa-dropbox text-warning"></span>
    #     <span class="sidebar-title text-muted">Bandwidth</span>
    #     <span class="pull-right mr20 text-muted">58%</span>
    #     <div class="progress progress-bar-xs mh20">
    #       <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 58%">
    #         <span class="sr-only">58% Complete</span>
    #       </div>
    #     </div>
    #   </a>
    # </li>
  end

  private

    def sidebar_simple_link(title, url, icon, label, a={}, text=nil, type='font')
      content_tag :a, class: a[:class], id: a[:id], href: url do
        concat send("icon_#{type}", icon)
        concat content_tag :span, title, class: text
        concat sidebar_title_tray(label)
      end
    end

    def icon_font(value)
      content_tag :span, nil, class: value
    end

    def icon_image(value)
      image_tag value
    end

    def sidebar_title_tray(label)
      content_tag :span, class: 'sidebar-title-tray' do
        content_tag :span, label, class: 'label label-xs bg-primary'
      end
    end

    def filter_css(base, opts)
      base.each do |key, value|
        base[key] = opts.include?(key) ? opts[key] : base[key]
      end
    end

    def append_attributes(static, opts)
      base = static.dup
      base.each do |k, sh|
        base[k] = append_attribute(base[k], opts[k]) if opts.include?(k)
      end
    end

    def append_attribute(static, opts)
      base = static.dup
      base.each do |key, value|
        base[key] = opts.include?(key) ? "#{value} #{opts[key]}" : value
        base[key] = base[key].strip if key == :id
      end
    end

end
