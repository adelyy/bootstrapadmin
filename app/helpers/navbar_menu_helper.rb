module NavbarMenuHelper

  def navbar_dropdown(title, &block)
    content_tag :li, class: 'dropdown menu-merge hidden-xs flipInX' do
      concat dropdown_link(title)
      concat content_tag :ul, class: 'dropdown-menu animated animated-short flipInX', role: 'menu', &block
    end
  end

  def navbar_dropdown_footer(title, url, icon)
    content_tag :li, class: 'dropdown-footer' do
      simple_link(title, url, icon, '')
    end
  end

  def navbar_divider
    content_tag :li, class: 'menu-divider hidden-xs' do
      content_tag :i, nil, class: 'fa fa-circle'
    end
  end

  def navbar_link(title, url, icon)
    content_tag :li, class: 'hidden-xs' do
      simple_link(title, url, icon)
    end
  end

  def metro_link(text, uri, color, icon)
    content_tag :a, href: uri, class: "metro-tile #{color} light" do
      concat content_tag :span, nil, class: "#{icon} text-muted"
      concat content_tag :span, text, class: 'metro-title'
    end
  end

  def dropdown_panel_fotter(options)
    content_tag :div, class: 'panel-footer text-center p7' do
     link_to options[:title], options[:path], class: 'link-unstyled'
    end
  end

  def dropdown_panel_body(&block)
    content_tag :div, class: 'panel-body scroller-pn pn scroll' do
      content_tag :div, class: 'scroller-content' do
        content_tag :ol, class: 'timeline-list', &block
      end
    end
  end

  def timeline_description(title, text, link='', path='#')
    ary = text.split(' ')
    phrase = title.nil? ? (ary[1..-1].join(' ')) : text
    title ||= ary[0]
    content_tag :div, nil, class: 'timeline-desc' do
      concat content_tag :b, "#{title} "
      concat content_tag :span, phrase
      concat link_to link, path
    end
  end

  def timeline_icon(tag_icon)
    content_tag :div, nil, class: 'timeline-icon bg-dark light' do
      content_tag :span, '', class: tag_icon
    end
  end

  def timeline_item(title, text, link='', extra_info='', path='#', tag_icon='fa fa-tags')
    content_tag :li, nil, class: 'timeline-item' do
      concat timeline_icon(tag_icon)
      concat timeline_description(title, text, link, path)
      concat content_tag :div, extra_info, class: 'timeline-date'
    end
  end

  def panel_icon(icon)
    content_tag :span, class: 'panel-icon' do
      content_tag :i, nil, class: icon
    end
  end

  def dropdown_panel_header(title, icon)
    content_tag :div, nil, class: 'panel-menu' do
      concat panel_icon(icon)
      concat content_tag :span, title, class: 'panel-title fw600'
    end
  end

  # title: string, menu: css_class, fotter: { title: string, path: '' }
  def dropdown_panel(title, options={}, &block)
    css = 'dropdown-menu dropdown-persist w350 animated animated-shorter fadeIn'
    content_tag :div, role: 'menu', class: "#{css} #{options[:menu]}" do
      content_tag :div, class: 'panel mbn' do
        concat dropdown_panel_header(title, options[:icon])
        concat dropdown_panel_body(&block)
        concat dropdown_panel_fotter(options[:fotter]) if options.include? :fotter
      end
    end
  end

  def topbar_button_icon(count, icon)
    button_tag data: { toggle: 'dropdown' },
      class: 'btn btn-sm dropdown-toggle', aria: { expanded: true } do
        concat content_tag :span, nil, class: "#{icon} fs14 va-m"
        concat content_tag :span, count, class: 'badge badge-danger'
    end
  end

  def topbar_button(count, icon='fa fa-bell-o', &block)
    content_tag :div, class: 'navbar-btn btn-group' do
      concat topbar_button_icon(count, icon)
      concat capture(&block)
    end
  end

  def topbar_menu_toggle(icon)
    link_to '#', class: 'topbar-menu-toggle btn btn-sm',
      data: {toggle: 'button'}, aria: {pressed: 'false'} do
      content_tag :span, nil, class: icon
    end
  end

  def topbar_dropmenu(&block)
    content_tag :div, id: 'topbar-dropmenu', class: 'alt topbar-menu-open' do
        content_tag :div, class: 'topbar-menu row', &block
    end
  end

  private

    def slink(title, url, icon, link_css)
      content_tag :a, class: link_css, href: url do
        concat content_tag :span, nil, class: icon
        concat title
      end
    end

    def simple_link(title, url, icon, link_css='toggle-active')
      span_css = icon +' fs18'
      slink(title, url, span_css, link_css)
    end

    def dropdown_link(title)
      content_tag :a, href:'#', class: 'dropdown-toggle',
        data: {toggle: 'dropdown'}, role: 'button', aria: {expanded: false}  do
        concat title
        concat content_tag :span, nil, class: 'caret caret-tp'
      end
    end

end
