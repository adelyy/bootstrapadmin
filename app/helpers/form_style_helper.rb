module FormStyleHelper

  def section_divider(text)
    content_tag :div, class: 'section-divider mb40' do
      content_tag :span, text
    end
  end

  def load_svg(path, ext='png', css='')
    image_tag "#{path}.svg", 'data-svg-fallback' => "#{path}.#{ext}", class: css
  end

  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do
        concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
        concat message
      end)
    end
    nil
  end

  def scroller_bar
    content_tag :div, class: 'scroller-bar' do
      content_tag :div, class: 'scroller-track'  do
        content_tag :div, nil, class: 'scroller-handle'
      end
    end
  end

  def chat_content(&block)
    content_tag :div, class: 'panel-body bg-light dark panel-scroller scroller-lg scroller-pn pn scroll' do
      concat content_tag :div, class: 'chat', &block
    end
  end

  def chat_message_title(title, time)
    content_tag :h5, class: 'media-heading' do
      concat content_tag :span, title
      concat content_tag :small, " #{time}"
    end
  end

  def chat_message_body(title, body, time)
    content_tag :div, class: 'media-body' do
      concat content_tag :span, nil, class: 'media-status'
      concat chat_message_title title, time
      concat content_tag :span, body
    end
  end

  def chat_message_profile(path)
    content_tag :div, class: 'media-left' do
      link_to '#' do
        image_tag path, class: 'media-object'
      end
    end
  end

  def chat_message(path, title, body, time, photo=nil)
    content_tag :div, class: 'media' do
      concat chat_message_profile path
      if photo.nil?
        concat chat_message_body title, body, time
      else
        concat chat_multimedia_body title, body, photo, time
      end
    end
  end

  def chat_multimedia_body(title, body, photo, time)
    content_tag :div, class: 'media-body' do
      concat content_tag :span, nil, class: 'media-status'
      concat chat_message_title title, time
      concat image_tag photo, class: 'img-responsive'
      concat content_tag :span, body
    end
  end

  def form_input_content(&block)
    content_tag :div, class: 'section' do
      content_tag :div, class: 'field', &block
    end
  end

  def absolute_simple_field(tag, attribute, options={})
    content_tag :div, class: 'section' do
      label_tag attribute, class: 'field prepend-icon' do
        concat send(tag, attribute, params[attribute],
          class: 'gui-input', placeholder: options[:placeholder])
        concat field_icon(options[:icon])
      end
    end
  end

  def absolute_custom_field(tag, attribute, value, options={})
    content_tag :div, class: 'section' do
      label_tag attribute, class: 'field prepend-icon' do
        concat send(tag, attribute, value,
          class: 'gui-input', placeholder: options[:placeholder])
        concat field_icon(options[:icon])
      end
    end
  end

  def form_image_content(btn_text, placeholder, &block)
    content_tag :div, class: 'section' do
      content_tag :label, class: 'field prepend-icon file' do
        concat content_tag :span, btn_text, class: 'button btn-primary'
        concat yield
        concat content_tag :input, nil, type: 'text', class: 'gui-input', placeholder: placeholder
        concat field_icon('fa fa-upload')
      end
    end
  end

  def body(css='', &block)
    css += ' ' + params[:controller].gsub('/', '-')
    css += ' ' + params[:action].gsub('/', '-')
    content_tag :body, class: css, &block
  end

  private

  def field_icon(icon)
    content_tag :label, class: 'field-icon' do
      content_tag :span, nil, class: icon
    end
  end

end
