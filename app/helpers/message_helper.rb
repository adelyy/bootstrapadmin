module MessageHelper

  def div_column(xs, xl, extra_css=nil, &block)
    css = "col-xs-#{xs} col-sm-#{xs} col-xl-#{xl} #{extra_css}"
    content_tag :div, class: css, &block
  end

  def panel_body(text1, unit, &block)
    content_tag :div, class: 'panel-body', &block
  end

  def panel_footer(text2, text4, &block)
    content_tag :div, class: 'panel-footer br-t p12' do
      content_tag :span, class: 'fs11', &block
    end
  end

  def counter_box_body(text1, unit)
    panel_body text1, unit do
      concat content_tag :h1, text1, class: 'fs30 mt5 mbn'
      concat content_tag :h6, unit, class: 'text-system'
    end
  end

  def counter_box_footer(text2, text4, icon='fa fa-user')
    panel_footer text2, text4 do
      concat content_tag :i, nil, class: "#{icon} pr5"
      concat content_tag :span, text2, nil
      concat content_tag :b, text4, nil
    end
  end

  def message_counter_box(text1, unit, text2, text4, options={})
    css = "panel panel-tile text-center br-a br-grey #{options[:css]}"
    content_tag :div, class: css do
      concat counter_box_body(text1, unit)
      concat counter_box_footer(text2, text4, options[:icon])
    end
  end

end
