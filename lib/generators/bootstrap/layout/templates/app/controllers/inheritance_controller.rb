class InheritanceController < ApplicationController
  respond_to :html
  helper_method :object_class, :sort_column, :sort_direction
  
  def index
    @objects = sort object_class.all
  end

  def show
    @object = find_object
  end

  def edit
    @object = find_object
  end

  def new
    @object = object_class.new
  end

  def create(redirect_path = nil)
    redirect_path ||= polymorphic_path(object_class)
    @object = object_class.new(permitted_params)
    if @object.save
      redirect_to redirect_path
    else
      respond_with @object
    end
  end

  def update(redirect_path = nil)
    redirect_path ||= polymorphic_path(object_class)
    @object = find_object
    if @object.update(permitted_params)
      redirect_to redirect_path
    else
      respond_with @object
    end
  end

  def destroy(redirect_path = nil)
    redirect_path ||= polymorphic_path(object_class)
    find_object.destroy
    redirect_to redirect_path
  end

  def object_class
    controller_name.classify.constantize
  end

  private

  def find_object
    controller_name.classify.constantize.find(params[:id])
  end

  def sort_column
    object_class.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end

  def sort(object)
    object.order("#{sort_column} #{sort_direction}")
  end
end
