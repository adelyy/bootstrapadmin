require 'rails/generators'

module Bootstrap
  module Generators
    class LayoutGenerator < ::Rails::Generators::Base
      source_root File.expand_path("../templates", __FILE__)
      desc "This generator generates layout file with navigation."
      argument :layout_name, :type => :string, :default => "application"

      attr_reader :app_name

      def generate_layout
        app = ::Rails.application
        @app_name = app.class.to_s.split("::").first
        ext = app.config.generators.options[:rails][:template_engine] || :erb
        template "app/views/layouts/layout.html.erb", "app/views/layouts/#{layout_name}.html.erb"
        copy_partials
      end

      def partials
        [
          "_header.html.erb", "_sidebar.html.erb",
          "_topbar.html.erb", "_topbar_dropdown.html.erb"
        ]
      end

      def copy_partials
        root = "app/views/layouts"
        directory "#{root}/sidebar", :mode => :preserve
        partials.each do |name|
          copy_file "#{root}/#{name}", "#{root}/#{name}"
        end
      end

    end
  end
end
